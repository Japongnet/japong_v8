 <?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<article class="post" id="post-<?php the_ID(); ?>">

			<h2><?php the_title(); ?></h2>

			<?php posted_on(); ?>

			<div class="entry">
				
				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>

			</div>
			
			<?php edit_post_link(__('Edit this entry','html5reset'), '<p>', '</p>'); ?>
			<![CDATA[
			<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="640" height="360"><param name="movie" value="https://thespitmagazine.com/wp-content/plugins/hdw-player-video-player-video-gallery/player.swf" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="flashvars" value="baseW=https://thespitmagazine.com&id=NDIwMjYwMS45Mg==" /><object type="application/x-shockwave-flash" data="https://thespitmagazine.com/wp-content/plugins/hdw-player-video-player-video-gallery/player.swf" width="640" height="360"><param name="movie" value="https://thespitmagazine.com/wp-content/plugins/hdw-player-video-player-video-gallery/player.swf" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="flashvars" value="baseW=https://thespitmagazine.com&id=NDIwMjYwMS45Mg==" /></object></object>
			]]>
			<![CDATA[
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="640" height="360"><param name="movie" value="https://thespitmagazine.com/wp-content/plugins/hdw-player-video-player-video-gallery/player.swf" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="flashvars" value="baseW=https://thespitmagazine.com&id=MzY3NzI3Ni42OA==" /><object type="application/x-shockwave-flash" data="https://thespitmagazine.com/wp-content/plugins/hdw-player-video-player-video-gallery/player.swf" width="640" height="360"><param name="movie" value="https://thespitmagazine.com/wp-content/plugins/hdw-player-video-player-video-gallery/player.swf" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="flashvars" value="baseW=https://thespitmagazine.com&id=MzY3NzI3Ni42OA==" /></object></object>
]]>
		</article>
		
		<?php comments_template(); ?>

		<?php endwhile; endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
